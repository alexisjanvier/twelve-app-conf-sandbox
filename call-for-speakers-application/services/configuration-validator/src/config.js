const convict = require('convict');

const isNotEmpty = val => {
    if (!val.trim()) {
        throw new Error('This environment variable cannot be empty');
    }
};

const config = convict({
    NODE_ENV: {
        default: '',
        doc: 'The application environment.',
        env: 'NODE_ENV',
        format: ['production', 'development', 'test'],
    },
    REDIS_URI: {
        default: '',
        doc: "The Redis's connexion URI",
        env: 'REDIS_URI',
        format: isNotEmpty,
    },
});

module.exports = config;

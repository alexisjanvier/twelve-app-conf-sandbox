export const users: string[] = ['fede', 'team', 'none'];

enum LevelType {
    league = 'league',
    team = 'team',
}

interface Level {
    code: string;
    label: string;
    type: LevelType;
    children?: Level[];
}

const data: Level[] = [
    {
        children: [
            {
                code: 'TLA',
                label: 'The leopard avengers',
                type: LevelType.team,
            },
            {
                code: 'LP',
                label: 'Les pétroleuses',
                type: LevelType.team,
            },
        ],
        code: 'RDC',
        label: 'Roller derby Caen',
        type: LevelType.league,
    },
    {
        children: [
            {
                code: 'LQD',
                label: 'Les Quedalles',
                type: LevelType.team,
            },
            {
                code: 'LBB',
                label: 'La Barbaque',
                type: LevelType.team,
            },
        ],
        code: 'PRG',
        label: 'Paris Rollergirls',
        type: LevelType.league,
    },
];

interface User {
    firstName: string;
    lastName: string;
}

interface UserData {
    filters: Level[];
    user: User;
}

export const getDataForUser = (user: string): UserData => {
    switch (user) {
        case `team`:
            return {
                filters: [data[0]],
                user: {
                    firstName: user,
                    lastName: 'Dragon',
                },
            };
        case `fede`:
            return {
                filters: data,
                user: {
                    firstName: user,
                    lastName: 'Dragon',
                },
            };
        default:
            return {
                filters: [],
                user: {
                    firstName: user,
                    lastName: 'Dragon',
                },
            };
    }
};

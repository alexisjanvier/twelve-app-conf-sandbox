import Koa from 'koa';

import { config } from './config';
import { getDataForUser, users } from './mockedUsers';

export const userHandler = async (ctx: Koa.Context) => {
    const user = ctx.cookies.get(config.cookieName, { signed: true });

    if (!user) {
        return ctx.throw(401);
    }

    ctx.body = getDataForUser(user);
};

export const loginHandler = async (ctx: Koa.Context) => {
    const { login, password } = ctx.request.body;

    if (!users.includes(login) || password !== 'password') {
        return ctx.throw(401);
    }

    ctx.cookies.set(config.cookieName, login, {
        httpOnly: true,
        maxAge: 3600000,
        signed: true,
    });

    ctx.body = getDataForUser(login);
};

export const logoutHandler = async (ctx: Koa.Context) => {
    ctx.cookies.set(config.cookieName, null, {
        httpOnly: true,
        maxAge: 0,
        signed: true,
    });

    ctx.body = { message: 'deconnected' };
};

export const authenticatedUserMiddleware = async (
    ctx: Koa.Context,
    next: () => Promise<any>,
) => {
    const user = ctx.cookies.get(config.cookieName, { signed: true });
    if (!user) {
        return ctx.throw(401);
    }
    ctx.user = user;

    await next();
};

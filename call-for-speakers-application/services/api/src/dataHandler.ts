import Koa from 'koa';

export const dataHandler = async (ctx: Koa.Context) => {
    ctx.body = {
        message: `This is data about league ${ctx.query.league} sended to ${
            ctx.user
        }`,
    };
};

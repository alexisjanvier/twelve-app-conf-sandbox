import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import Router from 'koa-router';
import signale from 'signale';

import {
    authenticatedUserMiddleware,
    loginHandler,
    logoutHandler,
    userHandler,
} from './authenticationHandler';
import { config } from './config';
import { dataHandler } from './dataHandler';
import { queuesMiddleware } from './queuing-middleware';

const app = new Koa();
app.keys = [config.cookiePassphrase];

app.use(bodyParser());
app.use(queuesMiddleware);

const router = new Router();

router.get('/user', userHandler);
router.post('/login', loginHandler);

router.get('/', ctx => {
    ctx.body = { message: 'Hello Call For Speakers App API' };
});
router.use(
    ['/', '/send-email', 'logout', 'dataviz'],
    authenticatedUserMiddleware,
);
router.get('/logout', logoutHandler);
router.get('/send-email', ctx => {
    ctx.queues.email.add({
        todo: 'send an email',
    });
    ctx.body = { message: 'ok' };
});
router.get('/dataviz', dataHandler);

app.use(router.routes());

app.listen(config.listingPort);

signale.log(`Server running on port ${config.listingPort}`);

import convict from 'convict';

const configSchema = convict({
    cookieName: {
        default: 'jsPlaygroundAJ',
        doc: 'API authentication cookie name',
        env: 'COKKIE_NAME',
        format: String,
    },
    cookiePassphrase: {
        default: 'thisIsLocalCookiePassphrase',
        doc: 'Passphrase used to encrypte cookies',
        env: 'COOKIE_PASSPHRASE',
        format: String,
    },
    env: {
        default: 'development',
        doc: 'The application environment.',
        env: 'NODE_ENV',
        format: ['production', 'development', 'test'],
    },
    listingPort: {
        default: 3000,
        doc: 'The Api port',
        env: 'API_PORT',
        format: Number,
    },
    redisUri: {
        default: 'redis://redis:6379',
        doc: 'Redis connexion.',
        env: 'REDIS_URI',
        format: String,
    },
});

configSchema.validate({ allowed: 'strict' });

export interface ConfigServer {
    cookieName: string;
    cookiePassphrase: string;
    env: string;
    listingPort: number;
    redisUri: string;
}

export const config = configSchema.getProperties() as ConfigServer;

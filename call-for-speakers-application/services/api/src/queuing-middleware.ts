import Queue from 'bull';
import Koa from 'koa';
import signale from 'signale';

import { config } from './config';

export const queuesMiddleware = async (
    ctx: Koa.Context,
    next: () => Promise<any>,
) => {
    try {
        ctx.queues = {
            email: new Queue('email', config.redisUri),
        };
    } catch (error) {
        signale.log(`Unable to init Bull queues: ${error.message}`, {
            error,
        });
    }

    await next();
};

import convict from 'convict';

const configSchema = convict({
    env: {
        default: 'development',
        doc: 'The application environment.',
        env: 'NODE_ENV',
        format: ['production', 'development', 'test'],
    },
    mailer: {
        emitter: {
            default: 'cfs@caen.camp',
            env: 'MAIL_EMITTER',
            format: String,
        },
        smtp: {
            auth: {
                pass: {
                    default: '',
                    env: 'SMTP_PASSWORD',
                    format: String,
                },
                user: {
                    default: '',
                    env: 'SMTP_USER',
                    format: String,
                },
            },
            host: {
                default: 'maildev',
                env: 'SMTP_HOST',
                format: String,
            },
            ignoreTLS: {
                default: true,
                env: 'SMTP_IGNORE_TLS',
                format: Boolean,
            },
            port: {
                default: 25,
                env: 'SMTP_PORT',
                format: 'int',
            },
            secure: {
                default: false,
                env: 'SMTP_SECURE',
                format: Boolean,
            },
        },
    },
    redisUri: {
        default: 'redis://redis:6379',
        doc: 'Redis connexion.',
        env: 'REDIS_URI',
        format: String,
    },
});

configSchema.validate({ allowed: 'strict' });

export interface ConfigServer {
    env: string;
    listingPort: number;
    mailer: {
        emitter: string;
        smtp: {
            auth: {
                pass: string;
                user: string;
            };
            host: string;
            ignoreTLS: boolean;
            port: number;
            secure: boolean;
        };
    };
    redisUri: string;
}

export const config = configSchema.getProperties() as ConfigServer;

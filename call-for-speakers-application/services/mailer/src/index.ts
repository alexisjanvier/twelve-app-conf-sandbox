import Queue from 'bull';

import { config } from './config';
import { sendMail } from './send-mail';

const mailQueue = new Queue('email', config.redisUri);
mailQueue.process(1, sendMail);

global.console.log('Call for speakers Twelve-Factor mailer is started!');

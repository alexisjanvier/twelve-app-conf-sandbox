import Queue from 'bull';
import { createTransport } from 'nodemailer';

import { config } from './config';

const mailerConfig = {
    host: config.mailer.smtp.host,
    ignoreTLS: config.mailer.smtp.ignoreTLS,
    port: config.mailer.smtp.port,
    secure: config.mailer.smtp.secure,
};

const transporter = createTransport(mailerConfig);
transporter.verify((err: Error, success: boolean) => {
    if (err) {
        global.console.log('Error on mail transporter configuration');
        throw err;
    }
});

interface MailBody {
    html: string;
    text: string;
}

const mailer = async (to: string, subject: string, { html, text }: MailBody) =>
    transporter.sendMail({
        from: config.mailer.emitter,
        html,
        subject,
        text,
        to,
    });

export const sendMail = async (job: Queue.Job): Promise<any> => {
    try {
        await mailer('foo@bar.com', "Thank's for your propal", {
            html: "<h1>Thank's for your propal.</h1>",
            text: "Thank's for your propal.",
        });
        global.console.log('mail sended');
        return Promise.resolve({
            result: 'ok',
        });
    } catch (error) {
        global.console.log('Error during email sending', error);
        throw error;
    }
};

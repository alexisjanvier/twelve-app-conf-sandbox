import React from 'react';
import { act, render, cleanup, waitForElement } from 'react-testing-library';
import 'jest-dom/extend-expect';
import expect from 'expect';
import axios from 'axios';

import { App } from './App';

afterEach(() => {
    cleanup();
});

jest.mock('axios');
jest.mock('./authentication', () => ({
    LoginPage: () => <h1>Login Page</h1>,
}));
jest.mock('./home', () => ({
    Home: () => <h1>Home Page</h1>,
}));
jest.mock('./federations/Choose', () => ({
    ChooseFederation: () => <h1>Choose Federation Page</h1>,
}));
jest.mock('./component/Navbar', () => ({
    NavBar: () => <h1>Navbar</h1>,
}));

describe('Main application', () => {
    it('should display waiting page before all', async () => {
        axios.get.mockReturnValue(Promise.reject({ status: 401 }));
        let component;
        act(() => {
            component = render(<App />);
        });
        const title = component.getByText('Waiting for authentication');
        expect(title).toBeInTheDocument();
    });

    it('should display login form if user is not authenticate', async () => {
        axios.get.mockReturnValue(Promise.reject({ status: 401 }));
        let component;
        act(() => {
            component = render(<App />);
        });
        expect(axios.get).toHaveBeenLastCalledWith('/api/user');
        const title = await waitForElement(() =>
            component.getByText('Login Page'),
        );
        expect(title).toBeInTheDocument();
    });

    it('should display federation choosing page if user as more than 1 federation', async () => {
        axios.get.mockReturnValue(
            Promise.resolve({
                data: {
                    user: { firstName: 'John', lastName: 'Doe' },
                    filters: [
                        {
                            code: 'A',
                            label: 'foo',
                            type: 'league',
                            children: [],
                        },
                        {
                            code: 'B',
                            label: 'bar',
                            type: 'league',
                            children: [],
                        },
                    ],
                },
            }),
        );
        const { getByText } = render(<App />);
        expect(axios.get).toHaveBeenLastCalledWith('/api/user');
        const title = await waitForElement(() =>
            getByText('Choose Federation Page'),
        );
        expect(title).toBeInTheDocument();
    });

    it('should display federation page if user as only 1 federation', async () => {
        axios.get.mockReturnValue(
            Promise.resolve({
                data: {
                    user: { firstName: 'John', lastName: 'Doe' },
                    filters: [
                        {
                            code: 'A',
                            label: 'foo',
                            type: 'league',
                            children: [],
                        },
                    ],
                },
            }),
        );
        const { getByText } = render(<App />);
        expect(axios.get).toHaveBeenLastCalledWith('/api/user');
        const title = await waitForElement(() => getByText('Home Page'));
        expect(title).toBeInTheDocument();
    });
});

import axios from 'axios';
import { useContext, useEffect, useReducer } from 'react';

import {
    generateApiCall,
    reducer,
    FETCH_ERROR,
    FETCH_START,
    FETCH_SUCCESS,
    FETCH_PARAMETERS_CHANGE,
} from './reducer';
import { AppContext } from '../../App';

export const useDataProvider = (url, emptyData) => {
    const { logout, federation } = useContext(AppContext);

    const [{ data, isLoading, isError, apiCall }, dispatch] = useReducer(
        reducer,
        {
            data: emptyData,
            isLoading: false,
            isError: false,
            apiCall: generateApiCall(federation, url),
        },
    );

    const fetchData = () => {
        dispatch({ type: FETCH_START });
        return axios
            .get(apiCall)
            .then(result => {
                dispatch({
                    type: FETCH_SUCCESS,
                    payload: { data: result.data },
                });
            })
            .catch(error => {
                if (error.code >= 401 && error.code <= 403) {
                    return logout();
                }
                dispatch({
                    type: FETCH_ERROR,
                    payload: { error },
                });
            });
    };

    useEffect(() => {
        dispatch({
            type: FETCH_PARAMETERS_CHANGE,
            payload: { federation, url },
        });
    }, [federation, url]);

    useEffect(() => {
        fetchData();
    }, [apiCall]);

    return { data, isLoading, isError };
};

import expect from 'expect';

import {
    generateApiCall,
    reducer,
    FETCH_START,
    FETCH_SUCCESS,
    FETCH_ERROR,
    FETCH_PARAMETERS_CHANGE,
} from './reducer';

const originalState = {
    data: null,
    isLoading: false,
    isError: false,
    apiCall: '/api',
};

describe('DataProvider Reducer', () => {
    describe('generateApiCall', () => {
        it('should generate generate url without query parameter if federation is null', () => {
            expect(generateApiCall(null, '/api')).toEqual('/api');
        });

        it('should generate generate url with query parameter if federation is not null', () => {
            expect(generateApiCall({ code: 'foo' }, '/api')).toEqual(
                '/api?league=foo',
            );
        });
    });

    describe('Reducer', () => {
        it('should return original state if action type is not managed', () => {
            expect(reducer(originalState, { type: 'wrongType' })).toBe(
                originalState,
            );
        });

        it('should return return mutated state on FETCH_START action', () => {
            expect(reducer(originalState, { type: FETCH_START })).toEqual({
                data: null,
                isLoading: true,
                isError: false,
                apiCall: '/api',
            });
        });

        it('should return return mutated state on FETCH_SUCCESS action', () => {
            expect(
                reducer(originalState, {
                    type: FETCH_SUCCESS,
                    payload: { data: { msg: 'ok' } },
                }),
            ).toEqual({
                data: { msg: 'ok' },
                isLoading: false,
                isError: false,
                apiCall: '/api',
            });
        });

        it('should return return mutated state on FETCH_ERROR action', () => {
            expect(
                reducer(originalState, {
                    type: FETCH_ERROR,
                    payload: { error: { msg: 'ko' } },
                }),
            ).toEqual({
                data: null,
                isLoading: false,
                isError: { msg: 'ko' },
                apiCall: '/api',
            });
        });

        it('should return return mutated state on FETCH_PARAMETERS_CHANGE action', () => {
            expect(
                reducer(originalState, {
                    type: FETCH_PARAMETERS_CHANGE,
                    payload: {
                        federation: { code: 'foo' },
                        url: '/api/another',
                    },
                }),
            ).toEqual({
                data: null,
                isLoading: false,
                isError: false,
                apiCall: '/api/another?league=foo',
            });
        });
    });
});

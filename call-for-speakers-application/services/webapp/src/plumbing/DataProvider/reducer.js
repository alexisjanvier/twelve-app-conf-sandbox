export const FETCH_START = 'FETCH_START';
export const FETCH_SUCCESS = 'FETCH_SUCCESS';
export const FETCH_ERROR = 'FETCH_ERROR';
export const FETCH_PARAMETERS_CHANGE = 'FETCH_PARAMETERS_CHANGE';

export const generateApiCall = (federation, url) =>
    federation ? `${url}?league=${federation.code}` : url;

export const reducer = (state, action) => {
    switch (action.type) {
        case FETCH_START:
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case FETCH_SUCCESS:
            return {
                ...state,
                data: action.payload.data,
                isLoading: false,
            };
        case FETCH_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: action.payload.error,
            };
        case FETCH_PARAMETERS_CHANGE:
            return {
                ...state,
                isLoading: false,
                isError: false,
                apiCall: generateApiCall(
                    action.payload.federation,
                    action.payload.url,
                ),
            };
        default:
            return state;
    }
};

import React, { Fragment, useState } from 'react';
import {
    act,
    fireEvent,
    render,
    cleanup,
    waitForElement,
} from 'react-testing-library';
import 'jest-dom/extend-expect';
import expect from 'expect';
import axios from 'axios';

import { useDataProvider } from './hook';
import { AppContext } from '../../App';

afterEach(() => {
    cleanup();
});

jest.mock('axios');

const AppContextProvider = ({ children, value }) => (
    <AppContext.Provider value={value}>{children}</AppContext.Provider>
);

const DataProvideHookConsumer = () => {
    const { data, isLoading, isError } = useDataProvider('/api/test-dp-hook', {
        message: 'default data',
    });
    if (isLoading) {
        return <p>isLoading</p>;
    }
    if (isError) {
        return <p>{isError.message}</p>;
    }
    return <p>{data.message}</p>;
};

const logoutMock = jest.fn();
const contextValue = {
    federation: { code: 'foo', label: 'League Foo' },
    logout: logoutMock,
};

const Tree = () => {
    const [activeContext, setActiveContext] = useState(contextValue);
    const changeContext = () =>
        setActiveContext({
            ...contextValue,
            federation: { code: 'bar', label: 'League Bar' },
        });
    return (
        <Fragment>
            <button onClick={changeContext}>update context</button>
            <AppContextProvider value={activeContext}>
                <DataProvideHookConsumer />
            </AppContextProvider>
        </Fragment>
    );
};

describe('DataProvider Hook', () => {
    it('should start to fetch first', async () => {
        axios.get.mockReturnValue(
            Promise.reject({ code: 500, message: 'error 500' }),
        );
        let component;
        act(() => {
            component = render(<Tree />);
        });
        const title = await waitForElement(() =>
            component.getByText('isLoading'),
        );
        expect(title).toBeInTheDocument();
    });

    it('should return error if api return an error', async () => {
        axios.get.mockReturnValue(
            Promise.reject({ code: 500, message: 'error 500' }),
        );
        let component;
        act(() => {
            component = render(<Tree />);
        });
        const error = await waitForElement(() =>
            component.getByText('error 500'),
        );
        expect(error).toBeInTheDocument();
    });

    it('should call logout if api return a 401 error', async () => {
        axios.get.mockReturnValue(
            Promise.reject({ code: 401, message: 'error 401' }),
        );
        let component;
        act(() => {
            component = render(<Tree />);
        });
        const loading = await waitForElement(() =>
            component.getByText('isLoading'),
        );
        expect(loading).toBeInTheDocument();
        expect(logoutMock).toHaveBeenCalled();
    });

    it('should return data sent by api success', async () => {
        axios.get.mockReturnValue(
            Promise.resolve({ data: { message: 'data from api' } }),
        );
        let component;
        act(() => {
            component = render(<Tree />);
        });
        const apiData = await waitForElement(() =>
            component.getByText('data from api'),
        );
        expect(apiData).toBeInTheDocument();
    });

    it('should change query parameters if federation change', async () => {
        axios.get.mockReturnValue(
            Promise.resolve({ data: { message: 'data from api' } }),
        );
        let component;
        act(() => {
            component = render(<Tree />);
        });
        expect(axios.get).toHaveBeenLastCalledWith(
            '/api/test-dp-hook?league=foo',
        );
        axios.get.mockReturnValue(
            Promise.resolve({
                data: { message: 'data from api, second call' },
            }),
        );
        act(() => {
            fireEvent.click(component.getByText('update context'));
        });
        expect(axios.get).toHaveBeenCalledWith('/api/test-dp-hook?league=bar');
    });
});

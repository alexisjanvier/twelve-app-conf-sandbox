import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { Button, Segment } from 'semantic-ui-react';

export class SendEmail extends Component {
    state = {
        sended: false,
        error: false,
    };

    handleSendEmail = event => {
        event.preventDefault();
        axios
            .get('/api/send-email')
            .then(() => this.setState({ sended: true }))
            .catch(() => this.setState({ error: true }));
    };

    render() {
        return (
            <Fragment>
                {this.state.error && (
                    <Segment color="red">Problem with email</Segment>
                )}
                {this.state.sended && (
                    <Segment color="green">Email sended</Segment>
                )}
                <Button onClick={event => this.handleSendEmail(event)}>
                    Send an Email
                </Button>
            </Fragment>
        );
    }
}

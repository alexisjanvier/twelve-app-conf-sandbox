import React from 'react';
import {
    render,
    fireEvent,
    cleanup,
    waitForElement,
} from 'react-testing-library';
import 'jest-dom/extend-expect';
import expect from 'expect';
import axios from 'axios';

import { SendEmail } from './SendEmail';

afterEach(() => {
    cleanup();
});

jest.mock('axios');

describe('SendEmail component', () => {
    it('should display a button', () => {
        const { getByText } = render(<SendEmail />);
        const button = getByText('Send an Email');
        expect(button).toBeInTheDocument();
    });

    it('should display message error if api call failed', async () => {
        const { getByText } = render(<SendEmail />);
        try {
            const firstError = getByText('Problem with email');
            expect(firstError).toEqual(
                'Error should not be displayed before click on button',
            );
        } catch (error) {
            expect('Error').not.toEqual('is not displayed');
        }

        axios.get.mockReturnValue(Promise.reject({ data: { message: 'ko' } }));
        fireEvent.click(getByText('Send an Email'));
        expect(axios.get).toBeCalledWith('/api/send-email');

        const errorMessage = await waitForElement(() =>
            getByText('Problem with email'),
        );
        expect(errorMessage).toBeInTheDocument();
    });

    it('should display success message if api call is a success', async () => {
        const { getByText } = render(<SendEmail />);
        try {
            expect(getByText('Email sended')).toEqual(
                'Success should not be displayed before click on button',
            );
        } catch (error) {
            expect('Success').not.toEqual('is not displayed');
        }

        axios.get.mockReturnValue(Promise.resolve({ data: { message: 'ok' } }));
        fireEvent.click(getByText('Send an Email'));
        expect(axios.get).toBeCalledWith('/api/send-email');

        const errorMessage = await waitForElement(() =>
            getByText('Email sended'),
        );
        expect(errorMessage).toBeInTheDocument();
    });
});

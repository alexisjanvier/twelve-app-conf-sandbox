import React from 'react';
import { Grid } from 'semantic-ui-react';

import { SendEmail } from './SendEmail';
import { TeamsList } from '../federations/List';
import { DataViz } from '../federations/Dataviz';

export const Home = () => (
    <>
        <Grid columns={2} padded>
            <Grid.Column>
                <TeamsList />
            </Grid.Column>
            <Grid.Column>
                <DataViz />
            </Grid.Column>
        </Grid>

        <SendEmail />
    </>
);

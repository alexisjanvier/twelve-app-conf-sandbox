import React from 'react';
import { Header } from 'semantic-ui-react';

const style = {
    h3: {
        marginTop: '1em',
    },
};
export const WaitingForAuthentication = () => (
    <Header
        as="h3"
        content="Waiting for authentication"
        style={style.h3}
        textAlign="center"
    />
);

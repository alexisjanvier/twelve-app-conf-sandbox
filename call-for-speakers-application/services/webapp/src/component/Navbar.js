import React, { Component } from 'react';
import { Container, Menu } from 'semantic-ui-react';

export class NavBar extends Component {
    state = { activeItem: 'home' };

    handleItemClick = (e, { name }) => this.setState({ activeItem: name });

    render() {
        return (
            <Container style={{ padding: '1rem 0' }}>
                <Menu inverted>
                    <Menu.Item name={this.props.federation.label} />
                    {this.props.onResetFederation && (
                        <Menu.Item
                            name="Change federation"
                            onClick={() => this.props.onResetFederation()}
                        />
                    )}
                    <Menu.Menu position="right">
                        <Menu.Item
                            name="logout"
                            active
                            onClick={() => this.props.onLogout()}
                        />
                    </Menu.Menu>
                </Menu>
            </Container>
        );
    }
}

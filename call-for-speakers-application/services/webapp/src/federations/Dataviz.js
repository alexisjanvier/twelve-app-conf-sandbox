import React, { Fragment } from 'react';
import { Header } from 'semantic-ui-react';

import { useDataProvider } from '../plumbing/DataProvider/hook';

export const DataViz = () => {
    const { data } = useDataProvider('/api/dataviz', null);
    return (
        <Fragment>
            <Header as="h2" content="Your data" textAlign="center" />
            <p>{data ? data.message : 'No data for the moment'}</p>
        </Fragment>
    );
};

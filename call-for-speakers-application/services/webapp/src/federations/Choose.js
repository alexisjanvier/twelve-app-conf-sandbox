import React from 'react';
import { Header } from 'semantic-ui-react';
import { Container, Segment } from 'semantic-ui-react';

const style = {
    h3: {
        marginTop: '1em',
    },
};
export const ChooseFederation = ({ federations, onChoose }) => (
    <Container>
        <Header
            as="h3"
            content="Choose a federation"
            style={style.h3}
            textAlign="center"
        />
        {federations.map(federation => (
            <Segment
                key={federation.code}
                style={{ cursor: 'pointer' }}
                onClick={() =>
                    onChoose({
                        code: federation.code,
                        label: federation.label,
                    })
                }
            >
                {federation.label}
            </Segment>
        ))}
        {federations.length < 1 && (
            <p>Sorry, You are not affiliated to any federation</p>
        )}
    </Container>
);

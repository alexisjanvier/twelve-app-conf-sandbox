import React, { Fragment, useState } from 'react';
import expect from 'expect';
import {
    cleanup,
    fireEvent,
    render,
    waitForElement,
} from 'react-testing-library';
import 'jest-dom/extend-expect';

import { TeamsList } from './List';
import { AppContext } from '../App';

const AppContextProvider = ({ children, value }) => (
    <AppContext.Provider value={value}>{children}</AppContext.Provider>
);

afterEach(() => {
    cleanup();
});

describe('TeamsList component', () => {
    it('should display "no teams" if needed', () => {
        const component = render(<TeamsList />);
        expect(
            component.getByText('There is no teams in this federation'),
        ).toBeInTheDocument();
    });

    it('Should display a list of all teams linked to selected federation', async () => {
        const contextValue = {
            federation: { code: 'foo' },
            filters: [
                {
                    code: 'foo',
                    children: [
                        { code: 'bar', label: 'bar' },
                        { code: 'bar2', label: 'bar2' },
                    ],
                },
            ],
        };
        const component = render(
            <AppContextProvider value={contextValue}>
                <TeamsList />
            </AppContextProvider>,
        );
        const teamName1 = await waitForElement(() =>
            component.getByText('bar'),
        );
        expect(teamName1).toBeInTheDocument();
        const teamName2 = await waitForElement(() =>
            component.getByText('bar2'),
        );
        expect(teamName2).toBeInTheDocument();
        try {
            const noTeams = component.getByText(
                'There is no teams in this federation',
            );
            expect(noTeams).toEqual('should not be in document');
        } catch (error) {
            expect('There is no teams').not.toEqual(
                'should not be in document',
            );
        }
    });

    it('Should update the team list when context values change', async () => {
        const contextValue = {
            federation: { code: 'fed1' },
            filters: [
                {
                    code: 'fed1',
                    children: [
                        { code: 'team1', label: 'team1' },
                        { code: 'team2', label: 'team2' },
                    ],
                },
                {
                    code: 'fed2',
                    children: [
                        { code: 'team3', label: 'team3' },
                        { code: 'team4', label: 'team4' },
                    ],
                },
            ],
        };
        const Tree = () => {
            const [activeContext, setActiveContext] = useState(contextValue);
            const changeContext = () =>
                setActiveContext({
                    ...contextValue,
                    federation: { code: 'fed2' },
                });
            return (
                <Fragment>
                    <button onClick={changeContext}>update context</button>
                    <AppContextProvider value={activeContext}>
                        <TeamsList />
                    </AppContextProvider>
                </Fragment>
            );
        };

        const component = render(<Tree />);
        const teamName1 = await waitForElement(() =>
            component.getByText('team1'),
        );
        expect(teamName1).toBeInTheDocument();
        const teamName2 = await waitForElement(() =>
            component.getByText('team2'),
        );
        expect(teamName2).toBeInTheDocument();

        fireEvent.click(component.getByText('update context'));
        expect(teamName1).not.toBeInTheDocument();
        expect(teamName2).not.toBeInTheDocument();
        const teamName3 = await waitForElement(() =>
            component.getByText('team3'),
        );
        expect(teamName3).toBeInTheDocument();
        const teamName4 = await waitForElement(() =>
            component.getByText('team4'),
        );
        expect(teamName4).toBeInTheDocument();
    });
});

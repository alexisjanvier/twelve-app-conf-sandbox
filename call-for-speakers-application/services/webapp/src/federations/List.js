import React, { Fragment, useContext } from 'react';
import { Header, Item } from 'semantic-ui-react';

import { AppContext } from '../App';

export const TeamsList = () => {
    const { federation, filters } = useContext(AppContext);
    const getTeams = (federation, filters) =>
        filters
            ? filters.find(filter => filter.code === federation.code).children
            : [];
    const teams = getTeams(federation, filters);
    return (
        <Fragment>
            <Header as="h2" content="Your teams" textAlign="center" />
            {(!filters || filters.length < 1) && (
                <p>There is no teams in this federation</p>
            )}
            <Item.Group relaxed>
                {teams.map(team => (
                    <Item key={team.code}>
                        <Item.Image
                            height="200"
                            width="200"
                            src={`http://placeimg.com/200/200/animals/grayscale?burn=${
                                team.code
                            }`}
                        />

                        <Item.Content verticalAlign="middle">
                            <Item.Header as="a">{team.label}</Item.Header>
                        </Item.Content>
                    </Item>
                ))}
            </Item.Group>
        </Fragment>
    );
};

import React from 'react';
import { render, cleanup } from 'react-testing-library';
import 'jest-dom/extend-expect';
import expect from 'expect';

import { DataViz } from './Dataviz';
import * as DataProvider from '../plumbing/DataProvider/hook';

afterEach(() => {
    cleanup();
});

jest.mock('../plumbing/DataProvider/hook');

describe('DataViz Component', () => {
    it('should display "no data" is while dataProvider return nothing', () => {
        DataProvider.useDataProvider.mockReturnValue({ data: null });
        const { getByText } = render(<DataViz />);
        expect(DataProvider.useDataProvider).toHaveBeenCalledWith(
            '/api/dataviz',
            null,
        );
        expect(getByText('No data for the moment')).toBeInTheDocument();
    });

    it('should display message returned by dataProvider', () => {
        DataProvider.useDataProvider.mockReturnValue({
            data: { message: 'mocked message' },
        });
        const { getByText } = render(<DataViz />);
        expect(DataProvider.useDataProvider).toHaveBeenCalledWith(
            '/api/dataviz',
            null,
        );
        expect(getByText('mocked message')).toBeInTheDocument();
    });
});

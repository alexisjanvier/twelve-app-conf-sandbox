import { Container, Form, Header, Segment } from 'semantic-ui-react';
import axios from 'axios';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

export class LoginPage extends Component {
    static propTypes = {
        onLogin: PropTypes.func.isRequired,
    };

    state = {
        login: '',
        password: '',
    };

    handleSubmit = event => {
        event.preventDefault();
        const { login, password } = this.state;

        axios
            .post('/api/login', { login, password })
            .then(response => {
                this.setState({ error: null, redirectToReferrer: true });
                this.props.onLogin(response.data);
            })
            .catch(error => {
                this.setState({ error });
            });
    };

    handleChange = name => event => {
        event.preventDefault();
        this.setState({ [name]: event.target.value });
    };

    render() {
        const { error, login, password } = this.state;

        return (
            <Container>
                <Form id="login_form" onSubmit={this.handleSubmit}>
                    <Header
                        as="h1"
                        style={{ margin: '3rem' }}
                        content="Authentication"
                        textAlign="center"
                    />
                    {error && (
                        <Segment color="red">
                            {error.code === 401
                                ? `Email ou mot de passe invalide`
                                : error.message}
                        </Segment>
                    )}
                    <Form.Input
                        fluid
                        label="login (fede, team or none)"
                        placeholder="Login"
                        value={login}
                        onChange={this.handleChange('login')}
                    />
                    <Form.Input
                        fluid
                        label="password (password)"
                        placeholder="Password"
                        type="password"
                        value={password}
                        onChange={this.handleChange('password')}
                    />
                    <Form.Button>Submit</Form.Button>
                </Form>
            </Container>
        );
    }
}

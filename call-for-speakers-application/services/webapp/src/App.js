import axios from 'axios';
import React, { useEffect, useReducer } from 'react';
import { Container } from 'semantic-ui-react';

import { ChooseFederation } from './federations/Choose';
import { Home } from './home';
import { LoginPage } from './authentication';
import { NavBar } from './component/Navbar';
import { WaitingForAuthentication } from './component/WaitingForAuthentication';
import {
    reducer,
    USER_AUTHENTICATED,
    USER_CHOOSE_FEDERATION,
    USER_LOGOUT,
    USER_RESET_FEDERATION,
    USER_NOT_AUTHENTICATED,
} from './appReducer';

export const AppContext = React.createContext({});

export const App = () => {
    const [
        { federation, filters, user, userHasBeenChecked },
        dispatch,
    ] = useReducer(reducer, {
        federation: null,
        filters: null,
        user: null,
        userHasBeenChecked: false,
    });

    const login = data => {
        const federationFromSession = window.sessionStorage.getItem(
            'federation',
        );
        dispatch({
            type: USER_AUTHENTICATED,
            payload: federationFromSession
                ? {
                      federation: JSON.parse(federationFromSession),
                      ...data,
                  }
                : data,
        });
    };

    const logout = () => {
        window.sessionStorage.clear();
        dispatch({ type: USER_LOGOUT });
    };

    const chooseFederation = federation => {
        window.sessionStorage.setItem(
            'federation',
            JSON.stringify({ label: federation.label, code: federation.code }),
        );
        dispatch({
            type: USER_CHOOSE_FEDERATION,
            payload: {
                federation,
            },
        });
    };

    const resetFederation = () => {
        dispatch({ type: USER_RESET_FEDERATION });
    };

    useEffect(() => {
        if (!userHasBeenChecked) {
            axios
                .get('/api/user')
                .then(response => login(response.data))
                .catch(error => {
                    dispatch({ type: USER_NOT_AUTHENTICATED });
                });
        }
    }, [userHasBeenChecked]);

    if (!userHasBeenChecked) {
        return <WaitingForAuthentication />;
    }

    if (userHasBeenChecked && !user) {
        return <LoginPage onLogin={login} />;
    }

    if (!federation) {
        return (
            <ChooseFederation
                federations={filters}
                onChoose={chooseFederation}
            />
        );
    }

    return (
        <AppContext.Provider
            value={{
                federation,
                filters,
                logout,
                user,
            }}
        >
            <Container>
                <NavBar
                    onResetFederation={
                        filters.length > 1 ? resetFederation : null
                    }
                    onLogout={logout}
                    federation={federation}
                />
                <Home />
            </Container>
        </AppContext.Provider>
    );
};

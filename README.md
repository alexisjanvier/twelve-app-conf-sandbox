# Twelve-Factor App Sandbox

This repository contains the code used to illustrate a number of questions, and the answers given at a given time, by [applications designed as a service](https://12factor.net).

### Validate your configuration for your twelve-factor applications

[Tag configuration_validation](https://gitlab.com/alexisjanvier/twelve-app-conf-sandbox/tags/configuration_validation)

> When we deliver our code as a docker image, how do we ensure hat their configuration will be correct?

This repository contains the code illustrating the blog post [How do you validate your configuration for your twelve-factor applications ?](https://marmelab.com/blog/2018/12/05/twelve-factor-applications-how-do-you-validate-your-configuration.html)
